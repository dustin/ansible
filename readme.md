# Ansible

## Getting started

```
ansible-galaxy install -r requirements.yml
```

### Examples

#### Aliases

```shell
alias ag='ansible-galaxy'
alias ap='ansible-playbook'
```
